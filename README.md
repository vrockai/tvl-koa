# tvl-koa

Backend service for an web-app creating vocabulary flash cards from movie
and television subtitles.

## Requirements

* Node.js 10 or higher

## Build

To build, execute:
```
$ npm install
```

## Test

To test, execute:
```
$ npm test 
```

## Serve

To start the server locally, execute:
```
$ npm start
```