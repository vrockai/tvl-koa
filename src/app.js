const Koa = require('koa');
const app = new Koa();

app.use(async ctx => {
    ctx.body = 'Hello TVL World!';
});

module.exports = app;
